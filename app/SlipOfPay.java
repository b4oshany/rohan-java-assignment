package app;


/**
 * Write a description of class SlipOfPay here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SlipOfPay implements PaySlip
{
    private int period;
    private String id, fname, lname, category;
    private double grossPay,incomeTax, netPay;
    public SlipOfPay(int period, String id, String fname,
    String lname, String category, double grossPay, double incomeTax, double netPay)
    {
        this.period  = period;
        this.id  = id;
        this.fname  = fname;
        this.lname  = lname;
        this.category = category;
        this.grossPay = grossPay;
        this.incomeTax = incomeTax;
        this.netPay = netPay;
    }
    public int getPeriod()
    {
         return this.period;
    }
    public String getId()
    {
         return this.id;
    }
    public String getFirstName()
    {
         return this.fname;
    }
    public String getLastName()
    {
         return this.lname;
    }
    public String getCategory()
    {
         return this.category;
    }
    public double getGrossPay()
    {
         return this.grossPay;       
    }
    public double getIncomeTax()
    {
         return this.incomeTax;
    }
    public double getNetPay()
    {
        return this.netPay;
    }
    public String toString()
    {
        String stringy ="\n________________________________________\n";
        stringy+=": month number: " + this.period +"\n";
        stringy+= "Employee: " + this.category +  this.id + " "
        + this.fname +" " + this.lname +"\n\n";

        stringy+= "Gross pay: " + this.grossPay
        +"\nIncome tax: " + this.incomeTax
        +"\nNet Pay: " + this.netPay;
        return stringy;
    }
}
