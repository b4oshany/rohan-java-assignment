package app;

 


/**
 * Write a description of interface PaySlip here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public interface PaySlip
{
 public int getPeriod();
 public String getId();
 public String getFirstName();
 public String getLastName();
 public String getCategory();
 public double getGrossPay();
 public double getIncomeTax();
 public double getNetPay();
}
