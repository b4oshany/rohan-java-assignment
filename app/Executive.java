package app;


/**
 * Write a description of class Executive here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.Scanner;
public class Executive extends Salaried
{
    private String category;
    private double bonus;
    public Executive(int id, String fName, String lName,
                       String address, String phone, double payRate)
    {
        super(id, fName, lName, address, phone, payRate);
        this.incomeTaxRate = 0.3;
        this.category  = "2";
        this.bonus = 0.0;
    }
    public void setBonus(double newBonus)
    {
        this.bonus = newBonus;
    }
    public double bonus()
    {
        return this.bonus;
    }
    public SlipOfPay pay(int month)
    {
         double incomeTax = (this.payRate() + this.bonus() )
         * (1.0 - incomeTaxRate);
         double netPay = this.payRate() + this.bonus() - incomeTax;
         return new SlipOfPay(month,"" + this.id(), this.fName(), this.lName(),
         this.category(), this.payRate(), incomeTax, netPay);
    }
}
