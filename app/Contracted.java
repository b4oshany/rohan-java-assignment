package app;


/**
 * Write a description of class Contractor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.Scanner;
public class Contracted extends Commissioned
{
    private double incomeTaxRate = 0.3;
    private double payRate;
    public void setContractCompletion(double contComp)
    {
        this.commissionDriver = contComp;
    }
    public Contracted(int id, String fName, String lName,
                       String address, String phone)
    {
        super(id, fName, lName, address, phone);
        this.incomeTaxRate = 0.3;
        this.category = "3";
        this.commissionRate = 1.0;
    }
}
