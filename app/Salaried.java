package app;

import java.util.Scanner;
import java.io.*;
/**
 * Write a description of class Salaried here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Salaried extends StaffMember
{
    protected double payRate;
    public final double THRESHOLD = 10000.0;
    public double payRate()
    {
        return this.payRate;
    }
    public void setPay(double newPay)
    {
        this.payRate = newPay;
    }
    public Salaried(int id, String fName, String lName,
                       String address, String phone, double payRate)
    {
        super(id, fName, lName, address, phone);
        this.payRate = payRate;
        this.category = "1";
        this.incomeTaxRate = 0.25;
    }
    public SlipOfPay pay(int month)
    {
         double taxableIncome = this.payRate - THRESHOLD;
         if (taxableIncome < 0.0)
         {
             taxableIncome = 0.0;
         }
         double incomeTax = taxableIncome * (1.0 - incomeTaxRate);
         double netPay = this.payRate() - incomeTax;
         return new SlipOfPay(month,"" + this.id(), this.fName(), this.lName(),
         this.category(), this.payRate(), incomeTax, netPay);
        
    }
    public String toString2()
    {
        String stringy = super.toString2() + "," + this.payRate();
        return stringy;
    }
    public void toTxtFile(PrintWriter outFile)
    {
        outFile.print(this.toString2());
        outFile.close();
    }
    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
}
