package app;

/**
 * Abstract class that specifies functionality for
 * a payroll application
 * @author COMP1161
 * @version 1.0a
 */
public abstract class RealPayroll
{
    private String companyName;
    protected StaffMember[] staffList;      //collection of staff members
    protected PaySlip[] payslips;          //collection of payslips
    
    /**
     * Constructor
     * @param name name of the company
     */
    public RealPayroll(String name)
    {
        this.companyName = name;
    }
    
    
    /** 
    * Calculate payroll and generate payslips for active employees for the specified period.
    * @param period month of the year (1 = January, etc.)
    */
    public void runPayroll(int month)
    {
        payslips = new PaySlip[staffList.length];
        for (int i = 0; i<staffList.length; i++)
            payslips[i] = staffList[i].pay(month);
    }
    
    /**
     * Retrieve a collection of pay slips for the current period
     */
    public abstract PaySlip[] getPaySlips();
    
    
    /**
     * Write payslip information to a text file
     * @param fileName name of the file to which the payslips should be saved
     */
    public abstract void postPaySlips(String fileName);
    
    /**
     * Locate and return a staff member object by first name and last name.
     * @return the located staff member or null if not found
     */
    public abstract StaffMember locateByName(String firstName, String lastName);
    
    /**
     * Locate and return a staff member object by id
     * @return the located staff member or null if not found
     */
    public abstract StaffMember locateById(String id);
}
