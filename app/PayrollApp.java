package app;

/**
 * Write a description of class PayrollApp here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.Arrays;
import java.util.Scanner;
import java.io.*;
public class PayrollApp extends RealPayroll
{
    /**
     * Constructor for objects of class PayrollApp
     */
    protected SlipOfPay[] payslips;
    public StaffMember[] staffList;
    private int month;
    public static StaffMember generateEmployee(String line)
    {
        Scanner s = new Scanner(line);
        s.useDelimiter(",");
        char a = line.charAt(0);
        line = line.substring(1,line.length()-1);
        StaffMember emp = new Salaried(0, "", "", "", "", 0.0);
        int id = s.nextInt();
        String fname = s.next();
        String lname = s.next();
        String address = s.next();
        String phone = s.next();
        double wages = s.nextInt();

        if (a=='1')
        {
            emp = new Salaried(id, fname, lname, address,
                phone, wages);
        }
        if (a=='2')
        {
            emp = new Executive(id, fname, lname, address,
                phone, wages);
        }
        if (a=='3')
        {
            emp = new Contracted(id, fname, lname, address,
                phone);
        }
        if (a=='4')
        {
            emp = new Commissioned(id, fname, lname, address,
                phone);
        }
        return emp;
    }

    public PayrollApp(String name, String file) throws IOException
    {
        super (name);
        this.month = 1;
        Scanner inn = new Scanner(new File(file));
        String in = "";
        String tempLine = "";
        this.staffList = new StaffMember[30];
        this.payslips = new SlipOfPay[30];
        int j = 0;
        for(int i = 0; inn.hasNextLine(); i++)
        {
            tempLine = inn.nextLine();
            if(tempLine.charAt(0)!='#')
            {
                System.out.println("test");
                staffList[j] = PayrollApp.generateEmployee(tempLine);
                j++;
            }
        }      
    }

    public StaffMember getStaff(int i)
    { 
        return staffList[i];
    }

    public void runPayroll(int month)
    {
        
        for (int i = 0; i<staffList.length;i++)
        {
            if(staffList[i] != null){                    
                if (staffList[i].category().equals("1")){
                    payslips[i] = ((Salaried)staffList[i]).pay(month);
                }else if (staffList[i].category().equals("2")){
                    payslips[i] = ((Executive)staffList[i]).pay(month);
                }else if (staffList[i].category().equals("3")){
                    payslips[i] = ((Contracted)staffList[i]).pay(month);
                }else if (staffList[i].category().equals("4")){
                    payslips[i] = ((Commissioned)staffList[i]).pay(month);
                }
            }
        }
    }

    public SlipOfPay[] getPaySlips()
    {
        return payslips;
    }

    public void postPaySlips(String fileName)
    {
        PrintWriter pw = null;
        try
        {
            pw = StaffMember.pw(fileName);
        }
        catch (IOException e)
        {
            System.out.println("Error");
        }
        if(pw != null){
            for (int i = 0; i < payslips.length; i++)
            {
               if(payslips[i] != null){
                    pw.println(payslips[i]);
                }
            }
            pw.close(); // write content of a file.
        }
        month++;
    }

    public int numStaff()
    {
        return staffList.length;
    }

    public StaffMember locateByName(String firstName, String lastName)
    {
        StaffMember a= null;
        //Arrays.sort(this.staffList);
        for(int i=0; i<staffList.length; i++)
        {
            if((staffList[i] != null) && (staffList[i].fName().toLowerCase()).equals(firstName.toLowerCase()) && (staffList[i].lName().toLowerCase()).equals(lastName.toLowerCase()))
            {

                return staffList[i];

            }

        }

            return null;
    }

    public StaffMember locateById(String id)
    {
        int tr = Integer.parseInt(id);
        StaffMember a=null;
        //Arrays.sort(this.staffList);
        for(int i=0; i<staffList.length; i++)
        {
            if ((staffList[i] != null) && tr == staffList[i].id())
            {
                return staffList[i];
            }

        }
        return null;
        
        
    }
    /*public StaffMember locateByName(String firstName, String lastName)
    {
    Arrays.sort(this.staffList);
    int index = Arrays.binarySearch(this.staffList ,new Executive(0, firstName, lastName, "", "", 0));
    return this.staffList[index];
    }
    
    public StaffMember locateById(String id)
    {
    Arrays.sort(this.staffList);
    int index = Arrays.binarySearch(this.staffList, new Executive(Integer.parseInt(id), "", "", "", "",0));
    return this.staffList[index];
    }*/

}