package app;

 import java.util.Scanner;
 import java.io.*;

//********************************************************************
//  StaffMember.java       Author: Lewis/Loftus
//
//  Represents a generic staff member.
//********************************************************************

abstract public class StaffMember implements Comparable<StaffMember>
{
   protected int id;            // Id number
   protected String firstName; // First name
   protected String lastName;  // Last name
   protected String address;    // Address
   protected String phone;      // Phone number
   protected double incomeTaxRate;
   protected String category;
   public String category()
    {
        return this.category;
    }   
   public int id()
   {
       return this.id;
   }
   public String fName()
   {
       return this.firstName;
   }
   public String lName()
   {
       return this.lastName;
   }
   public String address()
   {
       return this.address;
   }
   public String phone()
   {
       return this.phone;
   }
   private int compareToLname(StaffMember that)
    {
        String me = this.lName();
        String other = (that.lName());
        return me.compareTo(other);
    }
   private int compareToFname(StaffMember that)
   {
        String me = this.fName();
        String other = (that.fName());
        return me.compareTo(other);
    }
   public int compareTo(StaffMember that)
    {
        int result = this.compareToLname(that);
        if (result != 0)
        {
        return result;
        }
        else
        {
            result = this.compareToFname(that);
            if (result != 0)
            {
                return result;
            }
            else
            {
                return result;
            }
        }
    }

   //-----------------------------------------------------------------
   //  Constructor: Sets up this staff member using the specified
   //  information.
   //-----------------------------------------------------------------
   public StaffMember (int eId, String eFirstName, String eLastName,
                       String eAddress, String ePhone)
   {
      id = eId;
      firstName = eFirstName;
      lastName = eLastName;
      address = eAddress;
      phone = ePhone;
   }

   //-----------------------------------------------------------------
   //  Returns a string including the basic employee information.
   //-----------------------------------------------------------------
   public String toString()
   {
      String result = id + "\n"+
      "Name: " + lastName +", "+firstName+"\n";
      result += "Address: " + address + "\n";
      result += "Phone: " + phone;

      return result;
   }
   protected String toString2()
   {
       String stringy =this.category() + this.id() + "," + this.fName()
       + "," + this.lName() + "," + this.address()
       + "," + this.phone();
       return stringy;
   }
   //-----------------------------------------------------------------
   //  Derived classes must define the pay method for each type of
   //  employee.
   //-----------------------------------------------------------------
   public abstract PaySlip pay(int month);
   
   protected static PrintWriter pw (String file) throws IOException
   {
        FileWriter nfw = new FileWriter(file);
        BufferedWriter nbw = new BufferedWriter(nfw);
        PrintWriter out = new PrintWriter(nbw);
        return out;
   }
   //-------------------------------------------------------------------
   /* This object writes is data to a file
    * must be in format that can be loaded next time
    *@param outfile Scanner object for the file to be written
    */
   public abstract void toTxtFile(PrintWriter outFile);
}
