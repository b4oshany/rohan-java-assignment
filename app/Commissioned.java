package app;


/**
 * Write a description of class Commisisoned here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.Scanner;
import java.io.*;
public class Commissioned extends StaffMember
{
    protected double commissionRate;
    protected double commissionDriver;
    public void setSales(double sales)
    {
        this.commissionDriver = sales;
    }
    
    
    public double sales()
    {
        return this.commissionDriver;
    }
    public String category()
    {
        return this.category;
    }
    public Commissioned(int id, String fName, String lName,
                       String address, String phone)
    {
        super(id, fName, lName, address, phone);
        this.commissionRate = 0.05;
        this.incomeTaxRate = 0.25;
        this.category = "4";
    }
    public double grossPay()
    {
        double grossPay = commissionDriver * commissionRate;
        return grossPay;
    }
    public SlipOfPay pay(int month)
    {
         double grossPay = commissionDriver * commissionRate; 
         double incomeTax = grossPay * (1.0 - incomeTaxRate);
         double netPay = grossPay - incomeTax;
         return new SlipOfPay(month,"" + this.id(), this.fName(), this.lName(),
         this.category(), grossPay, incomeTax, netPay);
        
    }
    public String toString2()
    {
        String stringy = super.toString2() + "," + this.grossPay();
        return stringy;
    }
    public void toTxtFile(PrintWriter outFile)
    {
        outFile.print(this.toString2());
        outFile.close();
    }
}
