package ui;


/**
 * Write a description of class ListPanel here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.awt.*;
import javax.swing.*;
import app.*;
import java.util.*;
public class ListPanel extends JPanel
{
    // instance variables - replace the example below with your own
    private PayrollApp p;
    /**
     * Constructor for objects of class ListPanel
     */
    public ListPanel(PayrollApp p, JPanel out)
    {
        // initialise instance variables
        this.p = p;
        this.setLayout(new GridLayout(p.numStaff()+14,1));
        this.setBackground(Color.yellow);
        this.add(new JLabel("\tID\t\t         Name\t"));
        StaffMember[] s = new StaffMember [11];
        JButton[]b = new JButton [11];
        
        for (int i = 0; i< 11; i++)
            s[i]= p.getStaff(i);
        for (int i = 0; i< 11; i++)
            b[i] = new JButton("");
        for (int i = 0; i< 11; i++)
            this.add(b[i]);
            
        for (int i = 0; i< 11; i++)
            ((JButton)b[i]).setText("" +((StaffMember)s[i]).id() + ": " + 
            ((StaffMember)s[i]).fName()
            + " " + ((StaffMember)s[i]).lName());
    }

}
