package ui;


/**
 * Write a description of class PayrollGUI here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.awt.*;
import javax.swing.*;
import app.*;
public class PayrollGUI
{
   private PayrollApp p;
   public void run()
   {
        JFrame pichaFrame = new JFrame("Text Here");
        pichaFrame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        
        JPanel solarPanel = new JPanel();
        solarPanel.setLayout(new BorderLayout());
        UIPanel u = new UIPanel();
        solarPanel.add(u, BorderLayout.CENTER);
        JPanel out = new JPanel();
        solarPanel.add(new ListPanel(p, out),BorderLayout.WEST);
        
        solarPanel.setBackground(Color.blue);
        solarPanel.setPreferredSize(new Dimension(1024, 768));
        
        pichaFrame.getContentPane().add(solarPanel);
        pichaFrame.pack();
        pichaFrame.setVisible(true);
   }
   public PayrollGUI(PayrollApp p)
   {
        this.p = p;
   }
}
