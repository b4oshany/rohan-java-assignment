package ui;

/**
 * Write a description of class TextUICore here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import app.*;
import java.util.*;
public class TextUICore
{
    private String home,end; //changed
    private int in;
    private boolean contin = true;
    private Scanner scan;
    private boolean havePaid = false;

    PayrollApp payroll;

    public TextUICore(String homescreen, String escapeMessage)
    {
        this.home = homescreen;
        this.end = escapeMessage;
        this.scan = new Scanner(System.in);
    }
    
    public void setPayroll(PayrollApp p){
        this.payroll = p;
    }

    public String toString()
    {
        return home;
    }

    public void setDecision(int l)
    {
        this.in = l;
    }

    public boolean shouldContinue()
    {
        return this.contin;
    }

    public void reset()
    {
        this.in = 0;/////
    }

    private void exit()
    {
        this.contin = false;
    }

    public void doThese()
    {
        if (in==0)
        {
            reset();//
        }
        else if (in==1)
        {
            searchN();
        }

        else if (in==2)
        {
            searchID();
        }
        else if (in==3)
        {
            runIt();
        }
        else if (in==4)
        {
            if (havePaid)
            {
                System.out.println(view());
            }
            else
                System.out.println("Not Available yet - run payroll first.");
        }
         else if (in==5)
                 if (havePaid)
                 {
                     post();
                  }
                 else
                 System.out.println("Not Available yet - run payroll first.");
            else if (in==6 )//|| in==null || in.equals(""))
            {
                System.out.println(end);
                reset();
                this.exit();
            }
            else
            {
                System.out.println("That option is not valid, please choice a valid option(1-6)");

            }
        }
        
        public void updateStaffMember(StaffMember employee){
            System.out.printf("Do you want to update %s %s information? ", employee.fName(), employee.lName());
            String ln = scan.next();
            String choice;
            if(ln.toUpperCase().equals("Y")){
               
               if(employee instanceof Salaried || employee instanceof Executive){
                    System.out.printf("Enter the pay rate for %s %s:\n", employee.fName(), employee.lName());
                    double payrate = scan.nextDouble();
                    if(payrate > 0){
                        ((Salaried) employee).setPay(payrate);
                    }
                }else if(employee instanceof Contracted || employee instanceof Commissioned){
                    System.out.printf("Enter the amount of sales for %s %s:\n", employee.fName(), employee.lName());
                    double sales = scan.nextDouble();
                    if(sales > 0){
                        ((Commissioned) employee).setSales(sales);
                    }
                }
            }
        }

        public String searchN()
        {
            System.out.println("Please enter the persons lastname\n");
            String ln= scan.next();
            System.out.println("Please enter the persons firstname\n");
            String fn= scan.next();
            //System.out.println("Please enter the filename (eg. data.txt)\n");
            //String fl= scan.next();
            //PayrollApp a = new PayrollApp("",fl);
            StaffMember employee = payroll.locateByName(fn,ln);
            if(employee != null){
                System.out.println(employee);
                updateStaffMember(employee);
                return employee.toString();
            }else{
                System.out.printf("%s %s is not a staff member\n", fn, ln); 
                return "";
            }
        }

        public String searchID()
        {
            this.scan = new Scanner(System.in);
            System.out.println("Please enter the persons ID#\n");
            String id= scan.next();
            //System.out.println("Please enter the filename (eg. data.txt)\n");
            //String fl= scan.next();
            //PayrollApp a = new PayrollApp("",fl);
            StaffMember employee = payroll.locateById(id);
            if(employee != null){
                System.out.println(employee);
                updateStaffMember(employee);
                return employee.toString();
            }else{
                System.out.printf("No such staff member was found with the id %s\n", id); 
                return "";
            }
        }

        public void runIt()
        {
            System.out.println("Please enter the period in whitch you wish to run the payroll \n");
            int p = scan.nextInt();
            payroll.runPayroll(p); 
            havePaid = true;
        }

        public String view()
        {
            String s="";
            SlipOfPay payslip;
            for(int i=0; i<(payroll.getPaySlips()).length;i++){
                payslip = payroll.getPaySlips()[i];
                if(payslip != null){
                    s+= payslip.toString();
                }

           }
            return s;
        }

        public void post()
        {
            System.out.println("Please enter the file in which you wish to save the payslips \n");
            String w= scan.next();
            payroll.postPaySlips(w);
        }
    }
