package ui;

/**
 * Write a description of class PayRollTxtUI here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.Scanner;
import app.*;
public class PayrollTxtUI
{
    private PayrollApp p;
    
    int tempDecision;//changed to int
    Scanner in = new Scanner(System.in);

    String h = "1. Search by Name \n" +
        "2. Search by Id \n" +
        "3. Run Payroll \n" +
        "4. View Payslips \n" +
        "5. Post Payslips \n" +
        "6. Quit Program \n"; 

    String e= "GOOD BYE \n";
    
    TextUICore t = new TextUICore(h,e);

    public PayrollTxtUI(PayrollApp p)
    {
        this.p = p;
        t.setPayroll(p);
    }
    
    public void run()
    {
        while (t.shouldContinue())
        {
            System.out.println(t);
            tempDecision = in.nextInt();//changed to int cause thats whats in outline
            t.setDecision(tempDecision);
            t.doThese();
        }
        
        
    }
}
