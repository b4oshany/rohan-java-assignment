#lines that start with the hash symbol should be skipped
#This file contains sample employee data separated by commas
#Tokens are: category(number),id(number),first name(string),last name(string),address(string),phone(string),payrate
#First digit in id number is categor Categories are: 1=Salaried,2=Executive,3=Contracted,4=Commissioned
#payrate is 0 for commissioned and contracted staff since these values are to be set when the payroll is run
#Some addresses have several parts. The parts are separated by '|'.  These should be replaced by commas in the 
#address that is stored
1100,James, Johnson, 14 Great Lakes Road|Ontario, 416-777-2345,50000
1301,John,Brown,33 Brown Lane|Kingston 21,999-0001,30000
1302,Mary, Shaw,67 Carnegie Road|Portland,404-1111,45000
1403,Martha, Magnusson,3 Gethsemanie Lane|Jordan,555-8987,48000
1204,Joy,Christian,12 Gold Street|New Haven,444-4444,50000
1105,Carol, Baez,23 Woodstock Avenue|Woodstock,888-1962,47000
2887,William, Wozniack,9 Pentium Way|Apple Valley|Chicago,222-9768,0
2889,Linus,Torus,8 Kernel Street|Swapping OS,111-3333,0
3107,Pete,Marwick,15 Wall Street|Newark|New York,3456785,0
4207,Carla,Dunbar,The Pines|Woodlands,444-0001,0
4101,Cindy,Lauper,Girls Town|Function City,321-0321,0