import java.util.Scanner;
/**
 * Test class for payroll application
 * 
 * @author COMP1161 
 * @version 1.00
 */
import app.*;
import ui.*;
import java.io.*;
public class TestPayroll
{
    public static void main(String[] args) throws IOException
    {
        Scanner keyBd = new Scanner(System.in);
        int trials = 0;
        String response;
        PayrollApp payrollObject = new PayrollApp("Company Name", "payroll.txt");
        System.out.println("Welcome");
        do{
            System.out.print("Select(T)ext session, (G)UI session, or (E)xist the system: ");
            response = keyBd.nextLine();
            response.toUpperCase();
            if (response.equals("T"))
            {
                PayrollTxtUI txtUi = new PayrollTxtUI(payrollObject);
                txtUi.run();
                System.exit(0);
            }else if (response.equals("G")){
                PayrollGUI gui = new PayrollGUI(payrollObject);
                gui.run();
            }else if (response.equals("E")){
                System.exit(0);
            }else{   
                System.out.println("Incorrect option selected");
                trials += 1;
            }
        }while(trials != 0 && trials < 3);
    }
}
